cmake_minimum_required(VERSION 3.12)
project(ImageEditor)

set(CMAKE_CXX_STANDARD 14)

add_executable(ImageEditor main.cpp ppm/ppm.cpp ppm/ppm.h Image.cpp Image.h Vec3.h Filters/Filter.h Filters/FilterLinear.cpp Filters/FilterLinear.h Filters/FilterGamma.cpp Filters/FilterGamma.h Array.h Filters/FilterBlur.cpp Filters/FilterBlur.h Filters/FilterLaplace.cpp Filters/FilterLaplace.h Filters/FilterGrayscale.cpp Filters/FilterGrayscale.h Filters/FilterEdgeBlur.cpp Filters/FilterEdgeBlur.h)