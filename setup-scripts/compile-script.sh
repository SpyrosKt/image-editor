#!/bin/bash

cd ..

# compile the source files, link the library
# name the executable file "neg"
g++ main.cpp Image.cpp Filters/FilterLinear.cpp Filters/FilterGamma.cpp Filters/FilterBlur.cpp Filters/FilterLaplace.cpp -o filter -I ppm -L ppm -l ppm
