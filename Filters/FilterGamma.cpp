
#include "FilterGamma.h"

imaging::Image FilterGamma::operator<<(const imaging::Image &image) {
	Image finalImage = Image(image);
	vector<Color>* pImageBuffer = finalImage.getDataPtr();
	
	for(auto &pixel : *pImageBuffer)
		pixel = *this << pixel;
	
	return finalImage;
}

Color FilterGamma::operator<<(const Color &color) {
	Color result = Color(pow(color.r, gamma), pow(color.g, gamma), pow(color.b, gamma));
	
	result = result.clampToLowerBound(0);
	result = result.clampToUpperBound(1);
	
	return result;
}
