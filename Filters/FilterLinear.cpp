
#include "FilterLinear.h"

Image FilterLinear::operator<<(const Image &image) {
	Image finalImage = Image(image);
	vector<Color>* pImageBuffer = finalImage.getDataPtr();
	
	for(auto &pixel : *pImageBuffer)
		pixel = *this << pixel;
	
	return finalImage;
}

Color FilterLinear::operator<<(const Color &color) {
	Color temp = Color(color);
	Color result = (temp * a) + c;
	
	result = result.clampToLowerBound(0);
	result = result.clampToUpperBound(1);
	
	return result;
}
