
#ifndef FILTERLINEAR_H
#define FILTERLINEAR_H

#include "Filter.h"
#include "../Vec3.h"


class FilterLinear : public Filter {
protected:
	Color a, c;
public:
	FilterLinear() : a(Color(1, 1, 1)), c(Color(0, 0, 0)) {}
	
	FilterLinear(const Color &a, const Color &c) : a(a), c(c) {}
	
	FilterLinear(FilterLinear& filterLinear) : a(filterLinear.a), c(filterLinear.c) {}
	
	imaging::Image operator<<(const imaging::Image &image) override;
	
	Color operator<<(const Color& color);
};


#endif
