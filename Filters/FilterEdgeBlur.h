
#ifndef FILTEREDGEBLUR_H
#define FILTEREDGEBLUR_H

#include "FilterBlur.h"

class FilterEdgeBlur : public FilterBlur {
public:
	FilterEdgeBlur(unsigned int N) : FilterBlur(N) {}
	
	Image operator<<(const imaging::Image &image) override;
};


#endif
