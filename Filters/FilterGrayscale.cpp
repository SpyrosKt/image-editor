
#include "FilterGrayscale.h"

Image FilterGrayscale::operator<<(const Image &image) {
	Image finalImage = Image(image);
	vector<Color>* pImageBuffer = finalImage.getDataPtr();
	
	for(auto &pixel : *pImageBuffer)
		pixel = *this << pixel;
	
	return finalImage;
}

Color FilterGrayscale::operator<<(const Color &color) {
	double average = (color.r + color.g + color.b) / 3;
	
	Color result = Color(average, average, average);
	
	// Clamping shouldn't be necessary here - given that the original color's values are in bounds
	//result = result.clampToLowerBound(0);
	//result = result.clampToUpperBound(1);
	
	return result;
}
