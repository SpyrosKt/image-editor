
#ifndef FILTERBLUR_H
#define FILTERBLUR_H

#include <cmath>
#include "Filter.h"
#include "../Array.h"

class FilterBlur : public Filter, public math::Array<float> {
protected:
	unsigned int N;
public:
	FilterBlur() : N(1) {}
	
	FilterBlur(unsigned int N);
	
	FilterBlur(FilterBlur& filterBlur);
	
	imaging::Image operator<<(const imaging::Image &image) override;
};


#endif
