
#include "FilterLaplace.h"

FilterLaplace::FilterLaplace() {
	this->width = 3;
	this->height = 3;
	
	this->buffer = vector<float>({0, 1, 0, 1, -4, 1, 0, 1, 0});
}

Image FilterLaplace::operator<<(const imaging::Image &image) {
	Image finalImage = Image(image);
	
	Color newPixel;
	for(int i = 0; i < image.getWidth(); i++) {
		for(int j = 0; j < image.getHeight(); j++) {
			
			newPixel = Color(0, 0, 0);
			
			for(int m = -1; m <= 1; m++) {
				// skip pixel if it is out of bounds
				if(i + m < 0 || i + m > image.getWidth() - 1) continue;
				
				for(int n = -1; n <= 1; n++) {
					// skip pixel if it is out of bounds
					if(j + n < 0 || j + n > image.getHeight() - 1) continue;
					
					newPixel += image.getElement(i + m, j + n) * this->getElement(m + 1, n + 1);
				}
			}
			
			newPixel = newPixel.clampToUpperBound(1);
			newPixel = newPixel.clampToLowerBound(0);
			finalImage.setElement(i, j, newPixel);
			
		}
	}
	
	return finalImage;
}
