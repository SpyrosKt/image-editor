
#ifndef FILTERGAMMA_H
#define FILTERGAMMA_H

#include <cmath>
#include "Filter.h"

class FilterGamma : public Filter {
protected:
	float gamma;
public:
	FilterGamma() : gamma(1) {}
	
	explicit FilterGamma(float gamma) : gamma(gamma) {}
	
	FilterGamma(FilterGamma& filterGamma) : gamma(filterGamma.gamma) {}
	
	Image operator<<(const Image& image) override;
	
	Color operator<<(const Color& color);
};


#endif
