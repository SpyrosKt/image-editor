
#ifndef FILTERGRAYSCALE_H
#define FILTERGRAYSCALE_H

#include "Filter.h"


class FilterGrayscale : public Filter {
public:
	FilterGrayscale() = default;
	
	Image operator<<(const Image& image) override;
	
	Color operator<<(const Color& color);
};


#endif
