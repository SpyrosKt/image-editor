
#include "FilterBlur.h"


// this is used to ensure that N is an odd number
// there cannot be a neighborhood of ie. 6x6 whole pixels and a central one
unsigned int adjust(unsigned int &N);

FilterBlur::FilterBlur(unsigned int N) : N(adjust(N)), Array(adjust(N), adjust(N)) {
	for(float &i : this->buffer)
		i = 1 / pow(this->N, 2);
}

FilterBlur::FilterBlur(FilterBlur &filterBlur) : Array(filterBlur.width, filterBlur.height, filterBlur.buffer), N(filterBlur.N) {
	for(float &i : this->buffer)
		i = 1 / pow(this->N, 2);
}

imaging::Image FilterBlur::operator<<(const imaging::Image &image) {
	Image finalImage = Image(image);
	
	Color newPixel;
	for(int i = 0; i < image.getWidth(); i++) {
		for(int j = 0; j < image.getHeight(); j++) {
			
			newPixel = Color(0, 0, 0);
			
			int lowerMBound = - (int) N / 2;
			int upperMBound = N / 2;
			for(int m = lowerMBound; m <= upperMBound; m++) {
				// skip pixel if it is out of bounds
				if(i + m < 0 || i + m > image.getWidth() - 1) continue;
				
				
				int lowerNBound = - (int) N / 2;
				int upperNBound = N / 2;
				for(int n = lowerNBound; n <= upperNBound; n++) {
					// skip pixel if it is out of bounds
					if(j + n < 0 || j + n > image.getHeight() - 1) continue;
					
					newPixel += image.getElement(i + m, j + n) * this->getElement(m + N/2, n + N/2);
				}
			}
			
			finalImage.setElement(i, j, newPixel);
			
		}
	}
	
	return finalImage;
}

unsigned int adjust(unsigned int &N) {
	if(N % 2 == 0) {
		cout << "A blur filter with an area size of " << N << " cannot be created as " << N << " is even. "
			 << "A blur filter with an area of size " << N + 1 << " will be used instead." << endl;
		N++;
	}
	return N;
}
