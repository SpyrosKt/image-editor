
#ifndef FILTERLAPLACE_H
#define FILTERLAPLACE_H

#include "FilterBlur.h"

class FilterLaplace : public FilterBlur {
public:
	FilterLaplace();
	
	Image operator<<(const imaging::Image &image) override;
};


#endif
