
#include "FilterEdgeBlur.h"

Image FilterEdgeBlur::operator<<(const imaging::Image &image) {
	Image finalImage = Image(image);
	
	int validPixelCount;
	Color newPixel;
	for(int i = 0; i < image.getWidth(); i++) {
		for(int j = 0; j < image.getHeight(); j++) {
			
			validPixelCount = 0;	// count the in-bounds pixels
			newPixel = Color(0, 0, 0);
			
			int lowerMBound = - (int) N / 2;
			int upperMBound = N / 2;
			for(int m = lowerMBound; m <= upperMBound; m++) {
				// skip pixel if it is out of bounds
				if(i + m < 0 || i + m > image.getWidth() - 1) continue;
				
				
				int lowerNBound = - (int) N / 2;
				int upperNBound = N / 2;
				for(int n = lowerNBound; n <= upperNBound; n++) {
					// skip pixel if it is out of bounds
					if(j + n < 0 || j + n > image.getHeight() - 1) continue;
					
					newPixel += image.getElement(i + m, j + n);
					validPixelCount++;
				}
			}
			
			newPixel /= validPixelCount;
			finalImage.setElement(i, j, newPixel);
			
		}
	}
	
	return finalImage;
}
