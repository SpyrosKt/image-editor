
#include "Image.h"

bool imaging::Image::load(const string &filename, const string &format) {
	
	if(filename.length() == 0) {
		cerr << "No file name was provided" << endl;
		return false;
	}
	
	unsigned long extensionIndex = filename.find_last_of('.');
	if(extensionIndex == -1 || extensionIndex == filename.length() - 1) {
		cerr << "The file name provided does not contain a file extension" << endl;
		return false;
	}
	
	string extension = filename.substr(extensionIndex + 1);		// + 1 to skip the "."
	
	if(extension != format) {
		cerr << "The extension of the file " << filename << " is not supported" << endl;
		return false;
	}
	
	// the following are temporary variables created just to be passed as parameters
	int tempWidth = width;
	int tempHeight = height;
	float* contents = imaging::ReadPPM(filename.c_str(), &tempWidth, &tempHeight);
	
	if(contents == nullptr) {
		cerr << "The file could not be read" << endl;
		return false;
	}
	
	width = static_cast<unsigned int>(tempWidth);
	height = static_cast<unsigned int>(tempHeight);
	
	unsigned int sizeInPixels = width * height;
	buffer = vector<Color>(sizeInPixels);
	
	for(int i = 0; i < sizeInPixels; i++)
		buffer[i] = Color(contents[i * 3], contents[i * 3 + 1], contents[i * 3 + 2]);
	
	return true;
}

bool imaging::Image::save(const string &filename, const string &format) {
	// check the format
	if(format != "ppm") {
		cerr << "The format " << format << " is not recognized/supported." << endl;
		return false;
	}
	
	unsigned int sizeInPixels = width * height;
	float* contents = new float[sizeInPixels * 3];
	for(int i = 0; i < sizeInPixels; i++) {
		contents[i * 3] = buffer[i].r;
		contents[i * 3 + 1] = buffer[i].g;
		contents[i * 3 + 2] = buffer[i].b;
	}
	
	bool successfullyWritten = imaging::WritePPM(contents, width, height, filename.c_str());
	
	if(!successfullyWritten) {
		cerr << "Could not save image to " << filename << endl;
		return false;
	}
	
	return true;
}
