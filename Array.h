
#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>
#include <vector>


using namespace std;

namespace math{
	
	template <typename T>
	class Array {
	protected:
		std::vector<T> buffer;
		unsigned int width{}, height{};
	
	public:
		
		// metric accessors
		
		unsigned int getWidth() const {
			return width;
		}
		
		unsigned int getHeight() const {
			return height;
		}
		
		
		// data accessors
		
		vector<T> * getDataPtr() {
			return &buffer;
		}
		
		T getElement(unsigned int x, unsigned int y) const {
			if(x >= width  || y >= height)
				throw out_of_range("The coordinates " + to_string(x) + ", " + to_string(y) +
									" exceed the Array's dimensions (" + to_string(width) + ", " + to_string(height) +").");
			
			int index = y * this->width + x;
			return buffer[index];
		}
		
		
		// data mutators
		
		void setData(std::vector<T> newBuffer) {
			this->buffer = newBuffer;
		}
		
		void setElement(unsigned int x, unsigned int y, T &element) {
			if(x >= width  || y >= height)
				return;
			
			int index = y * this->width + x;
			buffer[index] = element;
		}
		
		
		// constructors and destructors
		
		Array() {
			this->width = 0;
			this->height = 0;
			this->buffer = std::vector<T>(0);
		}
		
		Array(unsigned int width, unsigned int height) : width(width), height(height) {
			this->buffer = std::vector<T>(width * height);
		}
		
		Array(unsigned int width, unsigned int height, std::vector<T> buffer) {
			if(width * height != buffer.size()) {
				cerr << "Inconsistent contents and image dimensions" << endl;
				Array();
			}
			else {
				this->width = width;
				this->height = height;
				this->buffer = buffer;
			}
		}
		
		Array(const Array &src) : width(src.width), height(src.height), buffer(src.buffer){}
		
		~Array() = default;;
		
		Array<T> &operator=(const Array<T> &right) {
			this->width = right.width;
			this->height = right.height;
			this->buffer = right.buffer;
			return *this;
		}
		
		T operator()(unsigned int x, unsigned int y) {
			return getElement(x, y);
		}
		
	};
	
}

#endif