
#include "ppm.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

float *imaging::ReadPPM(const char *filename, int *w, int *h) {
	
	ifstream imageStream;
	imageStream.open(filename);
	
	if(!imageStream.is_open()) {
		cerr << "The file could not be opened" << endl;
		return nullptr;
	}
	
	const int MAX_LINE_LENGTH = 100;
	
	char currentChar;
	char currentLine[MAX_LINE_LENGTH];
	string *attributes = new string[4];
	int currentAttribute = 0;
	
	// reads the header character by character
	while(imageStream.good() && currentAttribute <= 3) {
		
		imageStream.get(currentChar);
		
		// if a '#' is found, the rest of the line is skipped
		if(currentChar == '#')
			imageStream.getline(currentLine, MAX_LINE_LENGTH);
		
		// whenever a new line, tab or space character is found, it can be assumed that the current attribute has been read to its whole
		else if(currentChar == '\n' || currentChar == '\t' || currentChar == ' ')
			currentAttribute++;
			
		// add currentChar at back of the current attribute
		else
			attributes[currentAttribute].push_back(currentChar);
		
	}
	
	
	string type = attributes[0];
	int width = atoi(attributes[1].c_str());
	*w = width;
	int height = atoi(attributes[2].c_str());
	*h = height;
	int sizeInPixels = width * height;
	int maxValue = atoi(attributes[3].c_str());
	
	/*	Current checks
	 * 	1) Is type P6?
	 * 	2) Are width, height and maxValue truly just numbers?
	 * 	3) What's the max value? Is it between 0 and 255
	 *	...
	 *
	 */
	
	// this flag allows all of the following checks to be performed before returning a nullptr (if needed)
	// this way, all the error messages that need to be displayed, will be
	bool errorsDetected = false;
	
	if(type != "P6") {
		cerr << "The type " << type << " is not recognized/supported." << endl;
		errorsDetected = true;
	}
	
	if(attributes[1] != to_string(width) || attributes[2] != to_string(height)		// the attributes (dimensions) were not integers (or maybe not even a number)
	   || width <= 0 || height <= 0) {											// the attributes were integers, but not valid
		cerr << "The image dimensions declared in the header are invalid." << endl;
		errorsDetected = true;
	}
	
	if(attributes[3] != to_string(maxValue)			// similarly, in this case, the attribute is not an integer
	   || maxValue <= 0 || maxValue > 255) {	// the maxValue is not a valid integer
		cerr << "The max value of color components declared in the header is invalid." << endl;
		errorsDetected = true;
	}
	
	if(errorsDetected)
		return nullptr;
	
	
	int sizeInComponents = sizeInPixels * 3;
	float* contents = new float[sizeInComponents];
	
	
	std::vector<char> contentVector = vector<char>(std::istreambuf_iterator<char>(imageStream), std::istreambuf_iterator<char>());
	
	if(contentVector.size() != sizeInComponents) {
		cerr << "The file data does not comply to the information declared on the header" << endl;
		
		delete[] contents;		// before returning, delete contents
			
		return nullptr;
	}
	
	int currentComponent;
	for(int i = 0; i < contentVector.size(); i++) {
		currentComponent = (int) ((unsigned char) contentVector[i]);
		
		if(currentComponent > 255) {
			cerr << "Some color channel values are out of bounds" << endl;
			errorsDetected = true;
			break;
		}
		
		contents[i] = (float) currentComponent / maxValue;
	}
	
	if(errorsDetected) {
		delete[] contents;		// before returning, delete contents
		
		return nullptr;
	}
	
	return contents;
	
}

bool imaging::WritePPM(const float *data, int w, int h, const char *filename) {
	
	if(data == nullptr)
		return false;
	
	ofstream os;
	os.open(filename, ios::binary);
	
	if(!os.is_open()) {
		cerr << "Could not write to " << filename << endl;
		return false;
	}
	
	int maxValue = 255;
	int size = w * h * 3;
	
	os << "P6" << endl;
	os << w << " " << h << endl;
	os << maxValue << endl;
	
	int currentValue;
	for(int i = 0; i < size; i++) {
		currentValue = static_cast<int>(data[i] * maxValue);
		os << (unsigned char) currentValue;
	}
	return true;
}
