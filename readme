To use, type the following command:
 
filter [FILTERS] FILE
 
A FILE argument is required. File extension must be .ppm and format must be P6.
The new image is saved at the same directory as the original one.
 
    -f FILTER [OPTIONS]     Apply the filter specified by the FILTER parameter; the OPTIONS parameter's use
                            varies according to the filter
   
   
    FILTER can be "linear", "gamma", "blur" or "laplace"
   
    linear:     OPTIONS need to be six (6) floating point numbers: an 'a' for each color channel followed by
                a 'c' for each color channel (the color channels being R, G, B)
               
                Each channel's value is multiplied by the corresponding 'a' and increased by the corresponding
                'c'
                    Let pixel p(x, y) = <r, g, b>, the transformed pixel will be: p'(x, y) = <r', g', b'>,
                    where r' = aR * r + cR (and so on for g' and b')
                   
                    e.g.    filter -f linear 1 0.8 0.3 0.1 0.1 0.3 image.ppm
 
    gamma:      OPTIONS need to be one (1) floating point number: 'gamma'
               
                    Let pixel p(x, y) = <r, g, b>, the transformed pixel will be: p'(x, y) = <r', g', b'>,
                    where r' = r^gamma  (and so on for g' and b')
                   
                    e.g.    filter -f gamma 1.5 image.ppm
   
    blur:       OPTIONS need to be one (1) integer: 'N'
                   
                    Each pixel's new values depend on its neighborhood's pixels
                    A pixel's neighborhood is considered to be an NxN square around it (if the neighborhood
                    exceeds the image's edge, any out-f-bounds pixels are considered to be black)
                   
                    e.g.    filter -f blur 5 image.ppm
                   
    laplace:    The laplace filter uses no OPTIONS parameters
                   
                    The filter acts as an edge-detection filter
                   
                    e.g.    filter -f laplace image.ppm

    grayscale:  The grayscale filter uses no OPTIONS parameters

                    The filter converts the image to black & white format

                    e.g.    filter -f grayscale image.ppm