
#include <iostream>
#include "Image.h"
#include "Array.h"
#include "Vec3.h"
#include "Filters/FilterLinear.h"
#include "Filters/FilterGamma.h"
#include "Filters/FilterBlur.h"
#include "Filters/FilterLaplace.h"
#include "Filters/FilterGrayscale.h"


using namespace imaging;
using namespace std;

vector<Filter*> parseArguments(const vector<string>& arguments);
Filter* createFilter(vector<string> params);
FilterLinear* createFilterLinear(const vector<string> &params);
FilterGamma* createFilterGamma(const vector<string> &params);
FilterBlur* createFilterBlur(const vector<string> &params);
FilterLaplace* createFilterLaplace(const vector<string> &params);
FilterGrayscale* createFilterGrayscale(const vector<string> &params);

int main(int argc, char* argv[]) {
	
	if(argc == 1) {
		cerr << "No file name was provided" << endl;
		exit(1);
	}
	
	// create a vector from argv - skipping the first and last argument
	vector<string> argumentVector(0);
	for(int i = 1; i < argc - 1; i++)
		argumentVector.emplace_back(argv[i]);
	
	// load the image using the last argument
	string path = argv[argc - 1];
	string supportedFormat = "ppm";
	
	Image image = Image();
	bool successfullyLoaded = image.load(path, supportedFormat);
	
	if(!successfullyLoaded) {
		cout << path << " could not be read. Exiting" << endl;
		exit(1);
	}
	
	// create a filter chain using the arguments in-between
	vector<Filter*> filters = parseArguments(argumentVector);
	
	// apply the filters to the image
	for(Filter* filter : filters)
		image = *filter << image;
	
	// save the final image
	string saveDestination = string(path);
	saveDestination.insert(saveDestination.find_last_of('.'), "_filtered");
	image.save(saveDestination, "ppm");
	
	
	// delete all filter pointers
	for(Filter* filter : filters)
		delete filter;
	
	return 0;
	
}

vector<Filter*> parseArguments(const vector<string>& arguments) {
	vector<Filter*> filters(0);
	
	Filter* currentFilter = nullptr;
	vector<string> currentFilterParams(0);
	for(const auto &currentParam : arguments) {
		
		if(currentParam == "-f") {
			// if currentFilterParams is empty, then there is no need to clear it and create a filter before moving on
			// this is necessary for the first "-f" found and any "-f" not followed by parameters
			if(currentFilterParams.empty())
				continue;
			
			// Create filter using the current parameters
			currentFilter = createFilter(currentFilterParams);
			
			// Add the newly created filter to filters
			if(currentFilter != nullptr)
				filters.emplace_back(currentFilter);
			
			// clear currentFilterParams so it can be "populated" again from the start with the next filter's params
			currentFilterParams.clear();
		}
		else {
			// add the current parameter to the current filter's parameters
			currentFilterParams.emplace_back(currentParam);
		}
		
	}
	
	// create the last filter and add it to filters
	currentFilter = createFilter(currentFilterParams);
	if(currentFilter != nullptr)
		filters.emplace_back(currentFilter);
	
	
	return filters;
}

Filter* createFilter(vector<string> params) {
	if(params.empty())
		return nullptr;
	
	string filterType = params[0];
	if(filterType == "linear")
		return createFilterLinear(params);
	
	else if(filterType == "gamma")
		return createFilterGamma(params);
	
	else if(filterType == "blur")
		return createFilterBlur(params);
	
	else if(filterType == "laplace")
		return createFilterLaplace(params);
	
	else if(filterType == "grayscale")
		return createFilterGrayscale(params);
	
	else
		cerr << "Filter type " << filterType << " is not valid" << endl;
	
	return nullptr;
}

FilterLinear* createFilterLinear(const vector<string> &params) {
	if(params.size() != 7) {
		cerr << "Invalid number of parameters for filter type linear" << endl;
		return nullptr;
	}
	
	float aR, aG, aB, cR, cG, cB;
	
	try {
		aR = stof(params[1], nullptr);
		aG = stof(params[2], nullptr);
		aB = stof(params[3], nullptr);
		cR = stof(params[4], nullptr);
		cG = stof(params[5], nullptr);
		cB = stof(params[6], nullptr);
		
		return new FilterLinear(Color(aR, aG, aB), Color(cR, cG, cB));
	}
	catch(...) {
		cerr << "Filter parameters must consist of the filter type followed by numbers" << endl;
		return nullptr;
	}
}

FilterGamma* createFilterGamma(const vector<string> &params) {
	if(params.size() != 2) {
		cerr << "Invalid number of parameters for filter type gamma" << endl;
		return nullptr;
	}
	
	float gamma;
	
	try {
		gamma = stof(params[1], nullptr);
		
		return new FilterGamma(gamma);
	}
	catch(...) {
		cerr << "Filter parameters must consist of the filter type followed by numbers" << endl;
		return nullptr;
	}
}

FilterBlur* createFilterBlur(const vector<string> &params) {
	if(params.size() != 2) {
		cerr << "Invalid number of parameters for filter type blur" << endl;
		return nullptr;
	}
	
	unsigned int N;
	
	try {
		N = (unsigned int) stoul(params[1], nullptr);
	}
	catch(...) {
		cerr << "Filter parameters must consist of the filter type followed by numbers" << endl;
		return nullptr;
	}
	
	if(N < 0) {
		cerr << "Filter type blur should be used with an unsigned integer value" << endl;
		return nullptr;
	}
	
	return new FilterBlur(N);
}

FilterLaplace* createFilterLaplace(const vector<string> &params) {
	if(params.size() != 1) {
		cerr << "Invalid number of parameters for filter type laplace" << endl;
		return nullptr;
	}
	
	return new FilterLaplace();
}

FilterGrayscale* createFilterGrayscale(const vector<string> &params) {
	if(params.size() != 1) {
		cerr << "Invalid number of parameters for filter type grayscale" << endl;
		return nullptr;
	}
	
	return new FilterGrayscale();
}
